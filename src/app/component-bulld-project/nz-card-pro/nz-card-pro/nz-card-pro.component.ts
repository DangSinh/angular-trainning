import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-nz-card-pro',
  templateUrl: './nz-card-pro.component.html',
  styleUrls: ['./nz-card-pro.component.scss']
})
export class NzCardProComponent implements OnInit {
  @Input() urlImage: string;
  @Input() title: string;
  @Input() description: string;
  @Input() url: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  openNewTab(url: string): void {
    window.open(url, '_blank');
  }
}
