import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzCardProComponent } from './nz-card-pro/nz-card-pro.component';
import { NzCardModule } from 'ng-zorro-antd/card';


@NgModule({
  declarations: [NzCardProComponent],
  exports: [
    NzCardProComponent
  ],
  imports: [
    CommonModule,
    NzCardModule
  ]
})
export class NzCardProModule { }
