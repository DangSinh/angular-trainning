import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DomeComponent } from './dome/dome.component';

const routes: Routes = [
  { path: '', component: DomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DomRoutingModule {
}
