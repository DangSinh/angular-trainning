import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DomRoutingModule } from './dom-routing.module';
import { DomeComponent } from './dome/dome.component';


@NgModule({
  declarations: [DomeComponent],
  imports: [
    CommonModule,
    DomRoutingModule
  ]
})
export class DomModule { }
