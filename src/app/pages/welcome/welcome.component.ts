import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  documents = [
    {
      title: 'Trang chủ Angular',
      description: 'Trang chủ của Angular',
      url: 'https://angular.io/',
      urlImage: 'https://angular.io/assets/images/logos/angular/angular.svg'
    },
    {
      title: '100 Days Angular',
      description: 'Hướng dẫn, trainning Anguar của cộng đồng Angualar Việt Nam',
      url: 'https://github.com/angular-vietnam/100-days-of-angular',
      urlImage: 'https://avatars2.githubusercontent.com/u/66933599?s=200&v=4',
    },
    {
      title: 'Web Skills',
      description: 'Tổng hợp những kĩ năng cần thiết cho web developer',
      url: 'https://andreasbm.github.io/web-skills/?fbclid=IwAR3IeKTW_V6n2ZQdonCnZzTZUy2qHnQsOkIGqP2_W8R4_3LxfaY6sg-53oU',
      urlImage: 'https://raw.githubusercontent.com/andreasbm/web-skills/master/demo.gif',
    }
  ];
  library = [
    {
      title: 'Bootstrap ',
      description: 'Quickly design and customize responsive mobile-first sites with Bootstrap',
      url: 'https://getbootstrap.com/',
      urlImage: '../assets/images/bt.svg'
    },
    {
      title: 'Material',
      description: 'Material is a design system – backed by open-source code – that helps teams build high-quality digital experiences.',
      url: 'https://material.io/',
      urlImage: 'https://lh3.googleusercontent.com/IDHMnQrBxc5SRlLGWdUf50WHjIFyT6TIRsAhZhRir83-cnxwJIYBvyGArfN7t1c5_fK68SfvpUSyq6Hlijrpiw5AC3An4XlSz30f7w',
    },
    {
      title: 'Tailwind CSS',
      description: 'Rapidly build modern websites without ever leaving your HTML',
      url: 'https://andreasbm.github.io/web-skills/?fbclid=IwAR3IeKTW_V6n2ZQdonCnZzTZUy2qHnQsOkIGqP2_W8R4_3LxfaY6sg-53oU',
      urlImage: '../assets/images/tailwindcss-icon.svg',
    }, {
      title: 'The TypeScript Handbook',
      description: 'The TypeScript Handbook is intended to be a comprehensive document that explains TypeScript to everyday programmers. You can read the handbook by going from top to bottom in the left-hand navigation.',
      url: 'https://www.typescriptlang.org/docs/handbook/intro.html',
      urlImage: 'https://www.typescriptlang.org/images/index/ts-conf-keynote.jpg',
    }
  ];
  constructor() {
  }

  ngOnInit(): void {
  }

  openNewTab(url: string): void {
    window.open(url, '_blank');
  }
}
