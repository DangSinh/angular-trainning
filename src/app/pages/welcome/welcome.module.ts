import { NgModule } from '@angular/core';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './welcome.component';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { NzCardProModule } from '../../component-bulld-project/nz-card-pro/nz-card-pro.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [WelcomeRoutingModule, BreadcrumbsModule, NzCardProModule, NzGridModule, CommonModule],
  declarations: [WelcomeComponent],
  exports: [WelcomeComponent]
})
export class WelcomeModule { }
