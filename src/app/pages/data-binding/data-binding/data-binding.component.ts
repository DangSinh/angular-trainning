import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss']
})
export class DataBindingComponent implements OnInit {
  use = {
    name: 'DataBindingComponent',
  };
  vi = '<input type="text" [value]="user.name" />'.toString();
  eb = ' <button (click)="showInfo()">Click me!</button>'.toString();
  user = {
    name: 'Nguyễn Thị Hùng',
    mail: ''
  };
  eventMount: MouseEvent;

  constructor() {
  }

  ngOnInit(): void {
  }

  showInfo($event: MouseEvent): void {
    this.eventMount = $event;
    alert(this.user.name + '====' + this.user.mail);
  }
}
