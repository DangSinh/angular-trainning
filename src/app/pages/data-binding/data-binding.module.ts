import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataBindingRoutingModule } from './data-binding-routing.module';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [DataBindingComponent],
  imports: [
    CommonModule,
    DataBindingRoutingModule,
    BreadcrumbsModule,
    FormsModule
  ]
})
export class DataBindingModule { }
